mkdir /repositories
cd /repositories
rm -rf customer-product-rest-contoller-api # clone etmesi için

# git clone
git clone https://gitlab.com/bugracelik/customer-product-rest-contoller-api
cd customer-product-rest-contoller-api

# build, test, verify, package, install
mvn clean install

# kill
kill -9 $(lsof -t -i:5000)

# uzak sunucuya bağlanır ve orada java -jar komutunu çalıştırır
java -Dserver.port=5000 -jar /repositories/customer-product-rest-contoller-api/target/customer-product-rest-contoller-api.jar &
